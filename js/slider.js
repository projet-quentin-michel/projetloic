document.addEventListener('DOMContentLoaded', function() {
  showSlides(1, 'custom-slider');
  showSlides(1, 'custom-slider2'); 
});

function plusSlides(n, sliderClass) {
  showSlides(window['slideIndex_' + sliderClass] += n, sliderClass);
}

function currentSlide(n, sliderClass) {
  showSlides(window['slideIndex_' + sliderClass] = n, sliderClass);
}

var slider2Backgrounds = [
  { type: 'image', value: "url('C:/Users/steam/Documents/Projet Loic/image/fondE1.webp')" },
  { type: 'image', value: "url('C:/Users/steam/Documents/Projet Loic/image/fondE2.webp')" },
  { type: 'color', value: "#d5dae4" } 
];


function showSlides(n, sliderClass) {
  var slides = document.getElementsByClassName(sliderClass);
  var dots = document.getElementsByClassName(sliderClass + "-dot");
  if (!window['slideIndex_' + sliderClass]) {
    window['slideIndex_' + sliderClass] = 1;
  }
  if (n > slides.length) { window['slideIndex_' + sliderClass] = 1; }
  if (n < 1) { window['slideIndex_' + sliderClass] = slides.length; }
  for (var i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";  
  }
  for (var i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  if (slides.length > 0) {
    slides[window['slideIndex_' + sliderClass]-1].style.display = "block";  
  }
  if (dots.length > 0) {
    dots[window['slideIndex_' + sliderClass]-1].className += " active";
  }

  if (sliderClass === 'custom-slider2') {
    var bodyElement = document.body; 
    var currentElement = slider2Backgrounds[window['slideIndex_' + sliderClass] - 1];
    if (currentElement.type === 'image') {
      bodyElement.style.backgroundImage = currentElement.value;
      bodyElement.style.backgroundColor = ""; 
    } else if (currentElement.type === 'color') {
      bodyElement.style.backgroundColor = currentElement.value;
      bodyElement.style.backgroundImage = "none"; 
    }
  }
}
